import { CssClassMap } from '../../utils/declarations';
import {
  Component,
  ComponentInterface,
  Prop,
  h,
  Host,
  Event,
  EventEmitter
} from '@stencil/core';

/**
 * @slot - Content is placed in main slot.
 * @slot start - Content is placed to the left of the button.
 * @slot end - Content is placed to the right.
 */
@Component({
  tag: 'erni-button',
  styleUrl: 'erni-button.scss',
  shadow: true
})
export class UIButtonComponent implements ComponentInterface {
  /**
   * Contains a URL or a URL fragment that the hyperlink points to. If this property is set, an anchor tag will be rendered.
   *
   * @type {string}
   * @memberof UIButtonComponent
   */
  @Prop() href?: string;

  /**
   * Contains the target of the link, default value is _sef
   *
   * @type {string}
   * @memberof UIButtonComponent
   */
  @Prop({ reflect: true }) target?: string = '_self';

  /**
   * If true, full width button
   *
   * @type {boolean}
   * @memberof UIButtonComponent
   */
  @Prop() block?: boolean = false;

  /**
   * Type of the button style
   *
   * Deprecated values:
   * - `error` and `light` buttons are deprecated.
   * - `commercial` buttons are like old `secondary` buttons
   * - `secondary` buttons are like old `light` buttons
   *
   * @type {('primary' | 'secondary' | 'tertiary' | 'commercial' | 'commercial-inverse' | 'white-transparent' | 'dark')}
   * @memberof UIButtonComponent
   */
  @Prop() kind?:
    | 'primary'
    | 'secondary'
    | 'tertiary'
    | 'commercial'
    | 'commercial-inverse'
    | 'white-transparent'
    | 'dark' = 'primary';

  /**
   * Type of the button: anchor or button
   *
   * @type {('anchor' | 'button')}
   * @memberof UIButtonComponent
   */
  @Prop() type?: 'anchor' | 'button';

  /**
   * If true, the user cannot interact with the button.
   *
   * @memberof UIButtonComponent
   */
  @Prop({ reflect: true }) disabled = false;

  /**
   * This attributes determines the background and border color of the button.
   * By default, buttons have a solid background, but buttons could have a link transparent behaviour.
   *
   * @type {('solid' | 'link')}
   * @memberof UIButtonComponent
   */
  @Prop({ reflect: true }) fill?: 'solid' | 'link' = 'solid';

  /**
   * This attributes determines the background and border color of the button.
   * By default, buttons have a solid background, but buttons could have a link transparent behaviour.
   *
   * @type {boolean}
   * @memberof UIButtonComponent
   */
  @Prop({ reflect: true }) shape?: 'rounded' | 'squared' = null;

  /**
   * This attributes determines the size of the button.
   * By default, buttons have a medium size.
   *
   * @type {('sm' | ' md' | 'lg' | 'xl')}
   * @memberof UIButtonComponent
   */
  @Prop() size?: 'sm' | 'md' | 'lg' | 'xl' = 'md';

  /**
   * This attributes determines the type of width of button.
   * By default, buttons have a minimun size of 50px.
   *
   * @type {('extrawide' | 'fixed')}
   * @memberof UIButtonComponent
   */
  @Prop() widthType?: 'extrawide' | 'fixed' = null;

  /**
   * This attributes determines if button has extra horizontal padding.
   *
   * @type {boolean}
   * @memberof UIButtonComponent
   */
  @Prop() extraPadding?: boolean = false;

  /**
   * This attributes determines a11y capabilities
   * @memberof UIButtonComponent
   */
  @Prop({ reflect: true }) ariaLabelledby: string;
  @Prop({ reflect: true }) role: string = 'button';

  // Events

  /**
   * The event occurs when the button is clicked.
   *
   * @type {EventEmitter<void>}
   * @memberof UIButtonComponent
   */
  @Event() erniButtonClicked: EventEmitter;

  click(evt: UIEvent) {
    this.erniButtonClicked.emit({ value: true });
    return !this.disabled ? evt : false;
  }

  render() {
    // Define tag to be used in case of href is an anchor
    const TagType = this.href ? 'a' : ('button' as any);
    const attrs =
      TagType === 'button'
        ? { kind: this.kind }
        : { href: this.href, target: this.target };
    const cssClassName =
      this.href && this.type !== 'button'
        ? `erni-button--link-${this.kind}`
        : `erni-button--${this.fill}-${this.kind}`;

    const cssClasses: CssClassMap = {
      'erni-button--block': this.block,
      [cssClassName]: true,
      [`erni-button--${this.size}`]: true,
      [`erni-button--extrapadding`]: this.extraPadding,
      [`erni-button--${this.widthType}`]: !!this.widthType,
      [`erni-button--${this.shape}`]: !!this.shape
    };

    return (
      <Host
        tabIndex={0}
        class={{
          'erni-button--block': this.block
        }}
      >
        <TagType
          {...attrs}
          tabIndex={-1}
          class={cssClasses}
          disabled={this.disabled}
          onClick={(e: UIEvent) => this.click(e)}
          aria-invalid="false"
        >
          <slot name="start" />
          <slot />
          <slot name="end" />
        </TagType>
      </Host>
    );
  }
}
