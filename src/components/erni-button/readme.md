# erni-button

<readme-bar demo-url="erni-button--default"></readme-bar>

`erni-button` is Erni UI Component part of `@erni/components` , Buttons are essential to interactive forms, dialogs and other interfaces within erni projects. They allow the user to submit information or initiate an action.

## Install

**Using npm :**

`Web Component` or `React-Component`

```bash
# Web Component
npm install @erni/components --save
# React Component
npm install @erni/components/react --save
```

**Import in projects :**

Using Script tag(**_only Web Component_**):

```html
<script type="module" src="/build/erni-components.esm.js"></script>
<script nomodule src="/build/erni-components.js"></script>
```

Using Import in your app:

```js
// Web Component
import '@erni/components';
// React Component
import { ErniButton } from '@erni/components/react';
```

<!-- Auto Generated Below -->

## Usage

### React-component

**Use erni-button as react component**

```jsx
import { ErniButton } from '@erni/components/react'

<ErniButton>Hola</ErniButton>
<ErniButton kind="secondary" size="md">Button md</ErniButton>
```

### Web-component

### Sizes

::: demo [vanilla]

```html
<html>
  <!-- Small  -->
  <erni-button size="sm">Button sm</erni-button>
  <!-- Medium (Default) -->
  <erni-button size="md">Button md</erni-button>
  <!-- Large -->
  <erni-button size="lg">Button lg</erni-button>
  <!-- Extra Large -->
  <erni-button size="xl">Button xl</erni-button>
  <!-- Block -->
  <erni-button block>Button Block</erni-button>
</html>
```

:::

### Kind

::: demo [vanilla]

```html
<html>
  <!-- Primary  -->
  <erni-button kind="primary" size="md" disabled>Disabled</erni-button>
  <erni-button kind="primary">Button Default</erni-button>
  <erni-button kind="primary" size="sm">Button sm</erni-button>
  <erni-button kind="primary" size="md">Button md</erni-button>
  <erni-button kind="primary" size="lg">Button lg</erni-button>
  <erni-button kind="primary" block>Button Block</erni-button>
  <erni-button kind="primary" fill="link">Fill Link</erni-button>

  <!-- Secondary  -->
  <erni-button kind="secondary" size="md" disabled>Disabled</erni-button>
  <erni-button kind="secondary">Button Default</erni-button>
  <erni-button kind="secondary" size="sm">Button sm</erni-button>
  <erni-button kind="secondary" size="md">Button md</erni-button>
  <erni-button kind="secondary" size="lg">Button lg</erni-button>
  <erni-button kind="secondary" block>Button Block</erni-button>
  <erni-button kind="secondary" fill="link">Fill Link</erni-button>

  <!-- Tertiary  -->
  <erni-button kind="tertiary" size="md" disabled>Disabled</erni-button>
  <erni-button kind="tertiary">Button Default</erni-button>
  <erni-button kind="tertiary" size="sm">Button sm</erni-button>
  <erni-button kind="tertiary" size="md">Button md</erni-button>
  <erni-button kind="tertiary" size="lg">Button lg</erni-button>
  <erni-button kind="tertiary" block>Button Block</erni-button>
  <erni-button kind="tertiary" fill="link">Fill Link</erni-button>

  <!-- Commercial  -->
  <erni-button kind="commercial" size="md" disabled>Disabled</erni-button>
  <erni-button kind="commercial">Button Default</erni-button>
  <erni-button kind="commercial" size="sm">Button sm</erni-button>
  <erni-button kind="commercial" size="md">Button md</erni-button>
  <erni-button kind="commercial" size="lg">Button lg</erni-button>
  <erni-button kind="commercial" block>Button Block</erni-button>
  <erni-button kind="commercial" fill="link">Fill Link</erni-button>

  <!-- Commercial inverse  -->
  <erni-button kind="commercial-inverse" size="md" disabled>Disabled</erni-button>
  <erni-button kind="commercial-inverse">Button Default</erni-button>
  <erni-button kind="commercial-inverse" size="sm">Button sm</erni-button>
  <erni-button kind="commercial-inverse" size="md">Button md</erni-button>
  <erni-button kind="commercial-inverse" size="lg">Button lg</erni-button>
  <erni-button kind="commercial-inverse" block>Button Block</erni-button>
  <erni-button kind="commercial-inverse" fill="link">Fill Link</erni-button>

  <!-- White Transparent -->
  <erni-button kind="white-transparent" size="md" disabled>Disabled</erni-button>
  <erni-button kind="white-transparent">Button Default</erni-button>
  <erni-button kind="white-transparent" size="sm">Button sm</erni-button>
  <erni-button kind="white-transparent" size="md">Button md</erni-button>
  <erni-button kind="white-transparent" size="lg">Button lg</erni-button>
  <erni-button kind="white-transparent" block>Button Block</erni-button>
  <erni-button kind="white-transparent" fill="link">Fill Link</erni-button>

  <!-- Dark  -->
  <erni-button kind="dark" size="md" disabled>Disabled</erni-button>
  <erni-button kind="dark">Button Default</erni-button>
  <erni-button kind="dark" size="sm">Button sm</erni-button>
  <erni-button kind="dark" size="md">Button md</erni-button>
  <erni-button kind="dark" size="lg">Button lg</erni-button>
  <erni-button kind="dark" block>Button Block</erni-button>
  <erni-button kind="dark" fill="link">Fill Link</erni-button>
</html>
```

:::

### Button With Link

::: demo [vanilla]

```html
<html>
  <!-- Primary  -->
  <erni-button size="sm" href="http://www.google.com">Link sm</erni-button>
  <erni-button size="md" href="http://www.google.com">Link md</erni-button>
  <erni-button size="lg" href="http://www.google.com">Link lg</erni-button>
  <erni-button size="xl" href="http://www.google.com">Link lg</erni-button>

  <!-- secondary  -->
  <erni-button kind="secondary" size="sm" href="http://www.google.com">Link sm</erni-button>
  <erni-button kind="secondary" size="md" href="http://www.google.com">Link md</erni-button>
  <erni-button kind="secondary" size="lg" href="http://www.google.com">Link lg</erni-button>
  <erni-button kind="secondary" size="xl" href="http://www.google.com">Link xl</erni-button>

  <!-- tertiary  -->
  <erni-button kind="tertiary" size="sm" href="http://www.google.com">Link sm</erni-button>
  <erni-button kind="tertiary" size="md" href="http://www.google.com">Link md</erni-button>
  <erni-button kind="tertiary" size="lg" href="http://www.google.com">Link lg</erni-button>
  <erni-button kind="tertiary" size="xl" href="http://www.google.com">Link xl</erni-button>

  <!-- commercial  -->
  <erni-button kind="commercial" size="sm" href="http://www.google.com">Link sm</erni-button>
  <erni-button kind="commercial" size="md" href="http://www.google.com">Link md</erni-button>
  <erni-button kind="commercial" size="lg" href="http://www.google.com">Link lg</erni-button>
  <erni-button kind="commercial" size="xl" href="http://www.google.com">Link xl</erni-button>

  <!-- commercial inverse -->
  <erni-button kind="commercial-inverse" size="sm" href="http://www.google.com">Link sm</erni-button>
  <erni-button kind="commercial-inverse" size="md" href="http://www.google.com">Link md</erni-button>
  <erni-button kind="commercial-inverse" size="lg" href="http://www.google.com">Link lg</erni-button>
  <erni-button kind="commercial-inverse" size="xl" href="http://www.google.com">Link xl</erni-button>

  <!-- White Transparent  -->
  <erni-button kind="white-transparent" size="sm" href="http://www.google.com">Link sm</erni-button>
  <erni-button kind="white-transparent" size="md" href="http://www.google.com">Link md</erni-button>
  <erni-button kind="white-transparent" size="lg" href="http://www.google.com">Link lg</erni-button>
  <erni-button kind="white-transparent" size="xl" href="http://www.google.com">Link xl</erni-button>

  <!-- Dark  -->
  <erni-button kind="dark" size="sm" href="http://www.google.com">Link sm</erni-button>
  <erni-button kind="dark" size="md" href="http://www.google.com">Link md</erni-button>
  <erni-button kind="dark" size="lg" href="http://www.google.com">Link lg</erni-button>
  <erni-button kind="dark" size="xl" href="http://www.google.com">Link xl</erni-button>
</html>
```

:::

### Link

::: demo [vanilla]

```html
<html>
  <!-- Primary  -->
  <erni-button fill="link" size="sm">Link sm</erni-button>
  <erni-button fill="link" size="md">Link md</erni-button>
  <erni-button fill="link" size="lg">Link lg</erni-button>
  <erni-button fill="link" size="xl">Link lg</erni-button>

  <!-- secondary  -->
  <erni-button fill="link" kind="secondary" size="sm">Link sm</erni-button>
  <erni-button fill="link" kind="secondary" size="md">Link md</erni-button>
  <erni-button fill="link" kind="secondary" size="lg">Link lg</erni-button>
  <erni-button fill="link" kind="secondary" size="xl">Link xl</erni-button>

  <!-- tertiary  -->
  <erni-button fill="link" kind="tertiary" size="sm">Link sm</erni-button>
  <erni-button fill="link" kind="tertiary" size="md">Link md</erni-button>
  <erni-button fill="link" kind="tertiary" size="lg">Link lg</erni-button>
  <erni-button fill="link" kind="tertiary" size="xl">Link xl</erni-button>

  <!-- commercial  -->
  <erni-button fill="link" kind="commercial" size="sm">Link sm</erni-button>
  <erni-button fill="link" kind="commercial" size="md">Link md</erni-button>
  <erni-button fill="link" kind="commercial" size="lg">Link lg</erni-button>
  <erni-button fill="link" kind="commercial" size="xl">Link xl</erni-button>

  <!-- commercial inverse -->
  <erni-button fill="link" kind="commercial-inverse" size="sm">Link sm</erni-button>
  <erni-button fill="link" kind="commercial-inverse" size="md">Link md</erni-button>
  <erni-button fill="link" kind="commercial-inverse" size="lg">Link lg</erni-button>
  <erni-button fill="link" kind="commercial-inverse" size="xl">Link xl</erni-button>

  <!-- White Transparent  -->
  <erni-button fill="link" kind="white-transparent" size="sm">Link sm</erni-button>
  <erni-button fill="link" kind="white-transparent" size="md">Link md</erni-button>
  <erni-button fill="link" kind="white-transparent" size="lg">Link lg</erni-button>
  <erni-button fill="link" kind="white-transparent" size="xl">Link xl</erni-button>

  <!-- Dark  -->
  <erni-button fill="link" kind="dark" size="sm">Link sm</erni-button>
  <erni-button fill="link" kind="dark" size="md">Link md</erni-button>
  <erni-button fill="link" kind="dark" size="lg">Link lg</erni-button>
  <erni-button fill="link" kind="dark" size="xl">Link xl</erni-button>
</html>
```

:::

::: warning
Differences between fill `link` and a button with `href` are mostly in the way the will behavior and tag assigned to them. The `erni-button` with attribute `href` will be an HTMLElement `anchor` and visually has an underline text decoration. `erni-button`'s using the attribute `fill="link"` will be HTMLElement `button` but looking similar to a link, without borders but with button functionality.
:::

### Shape

::: demo [vanilla]

```html
<html>
  <!-- Rounded -->
  <erni-button size="md" shape="rounded">Rounded</erni-button>
  <!-- Squared -->
  <erni-button size="md" shape="squared">Squared</erni-button>
</html>
```

:::

### Width Type

::: demo [vanilla]

```html
<html>
  <!-- Fixed -->
  <erni-button size="md" width-type="fixed">Fixed</erni-button>
  <erni-button size="md" shape="rounded" width-type="fixed">Rounded Fixed</erni-button>
  <!-- Extrawide -->
  <erni-button size="md" width-type="extrawide">Extrawide</erni-button>
</html>
```

:::

### Extra Horizontal Padding

::: demo [vanilla]

```html
<html>
  <!-- Extra padding -->
  <erni-button size="md" extra-padding="true">Extra padding</erni-button>
</html>
```

:::

### Slots

::: demo [vanilla]

```html
<html>
  <erni-button kind="secondary" size="lg">
    <span slot="start">Prefix Text -- </span>
    Button Content
    <span slot="end"> -- Suffix Text</span>
  </erni-button>

  <erni-button kind="secondary" size="lg">
    <ui-icon name="previous" slot="start"></ui-icon>
    Button Content
    <ui-icon name="next" slot="end"></ui-icon>
  </erni-button>
</html>
```

:::

## Properties

| Property         | Attribute         | Description                                                                                                                                                                                          | Type                                                                                                              | Default     |
| ---------------- | ----------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ----------------------------------------------------------------------------------------------------------------- | ----------- |
| `ariaLabelledby` | `aria-labelledby` | This attributes determines a11y capabilities                                                                                                                                                         | `string`                                                                                                          | `undefined` |
| `block`          | `block`           | If true, full width button                                                                                                                                                                           | `boolean`                                                                                                         | `false`     |
| `disabled`       | `disabled`        | If true, the user cannot interact with the button.                                                                                                                                                   | `boolean`                                                                                                         | `false`     |
| `extraPadding`   | `extra-padding`   | This attributes determines if button has extra horizontal padding.                                                                                                                                   | `boolean`                                                                                                         | `false`     |
| `fill`           | `fill`            | This attributes determines the background and border color of the button. By default, buttons have a solid background, but buttons could have a link transparent behaviour.                          | `"link" \| "solid"`                                                                                               | `'solid'`   |
| `href`           | `href`            | Contains a URL or a URL fragment that the hyperlink points to. If this property is set, an anchor tag will be rendered.                                                                              | `string`                                                                                                          | `undefined` |
| `kind`           | `kind`            | Type of the button style Deprecated values: - `error` and `light` buttons are deprecated. - `commercial` buttons are like old `secondary` buttons - `secondary` buttons are like old `light` buttons | `"commercial" \| "commercial-inverse" \| "dark" \| "primary" \| "secondary" \| "tertiary" \| "white-transparent"` | `'primary'` |
| `role`           | `role`            |                                                                                                                                                                                                      | `string`                                                                                                          | `'button'`  |
| `shape`          | `shape`           | This attributes determines the background and border color of the button. By default, buttons have a solid background, but buttons could have a link transparent behaviour.                          | `"rounded" \| "squared"`                                                                                          | `null`      |
| `size`           | `size`            | This attributes determines the size of the button. By default, buttons have a medium size.                                                                                                           | `"lg" \| "md" \| "sm" \| "xl"`                                                                                    | `'md'`      |
| `target`         | `target`          | Contains the target of the link, default value is \_sef                                                                                                                                              | `string`                                                                                                          | `'_self'`   |
| `type`           | `type`            | Type of the button: anchor or button                                                                                                                                                                 | `"anchor" \| "button"`                                                                                            | `undefined` |
| `widthType`      | `width-type`      | This attributes determines the type of width of button. By default, buttons have a minimun size of 50px.                                                                                             | `"extrawide" \| "fixed"`                                                                                          | `null`      |

## Events

| Event                | Description                                                                                | Type               |
| -------------------- | ------------------------------------------------------------------------------------------ | ------------------ |
| `absisButtonClicked` | The event occurs when the button is clicked.                                               | `CustomEvent<any>` |
| `componentClicked`   | <span style="color:red">**[DEPRECATED]**</span> Use `absisButtonClicked` instead<br/><br/> | `CustomEvent<any>` |

## Slots

| Slot      | Description                                  |
| --------- | -------------------------------------------- |
|           | Content is placed in main slot.              |
| `"end"`   | Content is placed to the right.              |
| `"start"` | Content is placed to the left of the button. |

## CSS Custom Properties

| Name                                                | Description                                                    |
| --------------------------------------------------- | -------------------------------------------------------------- |
| `--erni-button-anchor-cursor`                       | Cursor for button links.                                       |
| `--erni-button-anchor-text-decoration`              | Text decoration for button link.                               |
| `--erni-button-anchor-text-decoration-active`       | Text decoration for button link when active.                   |
| `--erni-button-anchor-text-decoration-hover`        | Text decoration for button link when hover.                    |
| `--erni-button-anchor-text-style`                   | Text style for button link.                                    |
| `--erni-button-background-image`                    | Background image for button.                                   |
| `--erni-button-border-radius`                       | Border radius for buttons.                                     |
| `--erni-button-border-size`                         | Border size for buttons.                                       |
| `--erni-button-border-style`                        | Border style for buttons.                                      |
| `--erni-button-commercial`                          | Background color of commercial button kind.                    |
| `--erni-button-commercial-border`                   | Border color of commercial button kind.                        |
| `--erni-button-commercial-hover`                    | Background color of commercial button kind when hover.         |
| `--erni-button-commercial-hover-text-color`         | Text color of commercial button kind when hover.               |
| `--erni-button-commercial-inverse`                  | Background color of commercial-inverse button kind.            |
| `--erni-button-commercial-inverse-border`           | Border color of commercial-inverse button kind.                |
| `--erni-button-commercial-inverse-hover`            | Background color of commercial-inverse button kind when hover. |
| `--erni-button-commercial-inverse-hover-text-color` | Text color of commercial-inverse button kind when hover.       |
| `--erni-button-commercial-inverse-text-color`       | Text color of commercial-inverse button kind.                  |
| `--erni-button-commercial-text-color`               | Text color of commercial button kind.                          |
| `--erni-button-cursor`                              | Cursor for button.                                             |
| `--erni-button-dark`                                | Background color of dark button kind.                          |
| `--erni-button-dark-border`                         | Border color of dark button kind.                              |
| `--erni-button-dark-hover`                          | Background color of dark button kind when hover.               |
| `--erni-button-dark-text-color`                     | Text color of dark button kind.                                |
| `--erni-button-disabled-opacity`                    | Opacity for disabled button.                                   |
| `--erni-button-disabled-pointer-events`             | Pointer events for disabled buttons.                           |
| `--erni-button-extrapadding`                        | Horizontal padding for extrapadding button.                    |
| `--erni-button-extrawide-minimum-width`             | Min width for extrawide button.                                |
| `--erni-button-font-family`                         | Font family for button.                                        |
| `--erni-button-font-size-lg`                        | Font size for lg size button.                                  |
| `--erni-button-font-size-md`                        | Font size for md size button.                                  |
| `--erni-button-font-size-sm`                        | Font size for sm size button.                                  |
| `--erni-button-font-size-xl`                        | Font size for xl size button.                                  |
| `--erni-button-font-weight`                         | Font weight for button.                                        |
| `--erni-button-height-lg`                           | Height of lg size button.                                      |
| `--erni-button-height-md`                           | Height of md size button.                                      |
| `--erni-button-height-sm`                           | Height of sm size button.                                      |
| `--erni-button-height-xl`                           | Height of xl size button.                                      |
| `--erni-button-letter-spacing`                      | Letter spacing for button.                                     |
| `--erni-button-line-height`                         | Line height of button.                                         |
| `--erni-button-outline`                             | Outline for button.                                            |
| `--erni-button-padding-lg`                          | Padding for lg size button.                                    |
| `--erni-button-padding-md`                          | Padding for md size button.                                    |
| `--erni-button-padding-sm`                          | Padding for sm size button.                                    |
| `--erni-button-padding-xl`                          | Padding for xl size button.                                    |
| `--erni-button-primary`                             | Background color of primary button kind.                       |
| `--erni-button-primary-border`                      | Border color of primary button kind.                           |
| `--erni-button-primary-hover`                       | Background color of primary button kind when hover.            |
| `--erni-button-primary-hover-text-color`            | Text color of primary button kind when hover.                  |
| `--erni-button-primary-text-color`                  | Text color of primary button kind.                             |
| `--erni-button-rounded-border-radius`               | Border radius for rounded buttons.                             |
| `--erni-button-secondary`                           | Background color of secondary button kind.                     |
| `--erni-button-secondary-border`                    | Border color of secondary button kind.                         |
| `--erni-button-secondary-hover`                     | Background color of secondary button kind when hover.          |
| `--erni-button-secondary-hover-text-color`          | Text color of secondary button kind when hover.                |
| `--erni-button-secondary-text-color`                | Text color of secondary button kind.                           |
| `--erni-button-squared-border-radius`               | Border radius for squared buttons.                             |
| `--erni-button-tertiary`                            | Background color of tertiary button kind.                      |
| `--erni-button-tertiary-border`                     | Border color of tertiary button kind.                          |
| `--erni-button-tertiary-hover`                      | Background color of tertiary button kind when hover.           |
| `--erni-button-tertiary-hover-text-color`           | Text color of tertiary button kind when hover.                 |
| `--erni-button-tertiary-text-color`                 | Text color of tertiary button kind.                            |
| `--erni-button-text-decoration`                     | Text decoration for button links.                              |
| `--erni-button-transition-duration`                 | Transition duration for button when hover.                     |
| `--erni-button-transition-hover`                    | Transition hover.                                              |
| `--erni-button-transition-timing-function`          | Transition timing function when hover.                         |
| `--erni-button-white-space`                         | White space text for button.                                   |
| `--erni-button-white-transparent`                   | Background color of white-transparent button kind.             |
| `--erni-button-white-transparent-border`            | Border color of white-transparent button kind.                 |
| `--erni-button-white-transparent-hover`             | Background color of white-transparent button kind when hover.  |
| `--erni-button-white-transparent-hover-text-color`  | Text color of white-transparent button kind when hover.        |
| `--erni-button-white-transparent-text-color`        | Text color of white-transparent button kind.                   |
| `--erni-button-width`                               | Min width for buttons.                                         |

## Dependencies

### Used by

- [ui-dropzone](../../media-elements/ui-dropzone)
- [ui-pagination](../../visualization-elements/ui-pagination)
- [ui-video-player](../../media-elements/ui-video-player)

### Graph

```mermaid
graph TD;
  ui-dropzone --> erni-button
  ui-pagination --> erni-button
  ui-video-player --> erni-button
  style erni-button fill:#f9f,stroke:#333,stroke-width:4px
```

---

**@absis-components/ui-core-elements : 0.1.8**
