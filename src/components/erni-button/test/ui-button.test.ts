import { UIButtonComponent } from '../erni-button';
import { newSpecPage } from '@stencil/core/testing';

describe('odp-button', () => {
  it('builds', () => {
    expect(new UIButtonComponent()).toBeTruthy();
  });

  it('properties should have the proper values assigned', () => {
    const button = new UIButtonComponent();

    expect(button.ariaLabelledby).toBeUndefined();
    expect(button.block).toBe(false);
    expect(button.target).toBe('_self');
    expect(button.kind).toBe('primary');
    expect(button.disabled).toBe(false);
    expect(button.fill).toBe('solid');
    expect(button.size).toBe('md');
    expect(button.extraPadding).toBe(false);

    expect(button.href).toBe(undefined);
    expect(button.role).toBe('button');
    expect(button.shape).toBe(null);
    expect(button.target).toBe('_self');
    expect(button.type).toBe(undefined);
    expect(button.widthType).toBe(null);
  });

  it('Renders Primary Button', async done => {
    const cmpRender = await newSpecPage({
      components: [UIButtonComponent],
      html: `<erni-button kind="primary">Render Test</erni-button>`
    });

    await expect(cmpRender.root)
      .toEqualHtml(`<erni-button kind="primary" fill=\"solid\" role=\"button\" tabindex=\"0\" target=\"_self\">
      <mock:shadow-root>
        <button aria-invalid=\"false\" class=\"erni-button--solid-primary erni-button--md\" kind=\"primary\" tabindex=\"-1\">
          <slot name=\"start\"></slot>
          <slot></slot>
          <slot name=\"end\"></slot>
        </button>
      </mock:shadow-root>
      Render Test
    </erni-button>`);

    done();
  });

  it('Renders Secondary Button', async done => {
    const cmpRender = await newSpecPage({
      components: [UIButtonComponent],
      html: `<erni-button kind="secondary">Render Test</erni-button>`
    });

    await expect(cmpRender.root)
      .toEqualHtml(`<erni-button kind="secondary" fill=\"solid\" role=\"button\" tabindex=\"0\" target=\"_self\">
      <mock:shadow-root>
        <button aria-invalid=\"false\" class=\"erni-button--solid-secondary erni-button--md\" kind=\"secondary\" tabindex=\"-1\">
          <slot name=\"start\"></slot>
          <slot></slot>
          <slot name=\"end\"></slot>
        </button>
      </mock:shadow-root>
      Render Test
    </erni-button>`);

    done();
  });

  it('Renders Tertiary Button', async done => {
    const cmpRender = await newSpecPage({
      components: [UIButtonComponent],
      html: `<erni-button kind="tertiary">Render Test</erni-button>`
    });

    await expect(cmpRender.root)
      .toEqualHtml(`<erni-button kind="tertiary" fill=\"solid\" role=\"button\" tabindex=\"0\" target=\"_self\">
      <mock:shadow-root>
        <button aria-invalid=\"false\" class=\"erni-button--solid-tertiary erni-button--md\" kind=\"tertiary\" tabindex=\"-1\">
          <slot name=\"start\"></slot>
          <slot></slot>
          <slot name=\"end\"></slot>
        </button>
      </mock:shadow-root>
      Render Test
    </erni-button>`);

    done();
  });

  it('Renders Commercial Button', async done => {
    const cmpRender = await newSpecPage({
      components: [UIButtonComponent],
      html: `<erni-button kind="commercial">Render Test</erni-button>`
    });

    await expect(cmpRender.root)
      .toEqualHtml(`<erni-button kind="commercial" fill=\"solid\" role=\"button\" tabindex=\"0\" target=\"_self\">
      <mock:shadow-root>
        <button aria-invalid=\"false\" class=\"erni-button--solid-commercial erni-button--md\" kind=\"commercial\" tabindex=\"-1\">
          <slot name=\"start\"></slot>
          <slot></slot>
          <slot name=\"end\"></slot>
        </button>
      </mock:shadow-root>
      Render Test
    </erni-button>`);

    done();
  });

  it('Renders Commercial Inverse Button', async done => {
    const cmpRender = await newSpecPage({
      components: [UIButtonComponent],
      html: `<erni-button kind="commercial-inverse">Render Test</erni-button>`
    });

    await expect(cmpRender.root)
      .toEqualHtml(`<erni-button kind="commercial-inverse" fill=\"solid\" role=\"button\" tabindex=\"0\" target=\"_self\">
      <mock:shadow-root>
        <button aria-invalid=\"false\" class=\"erni-button--solid-commercial-inverse erni-button--md\" kind=\"commercial-inverse\" tabindex=\"-1\">
          <slot name=\"start\"></slot>
          <slot></slot>
          <slot name=\"end\"></slot>
        </button>
      </mock:shadow-root>
      Render Test
    </erni-button>`);

    done();
  });

  it('Renders White transparent Button', async done => {
    const cmpRender = await newSpecPage({
      components: [UIButtonComponent],
      html: `<erni-button kind="white-transparent">Render Test</erni-button>`
    });

    await expect(cmpRender.root)
      .toEqualHtml(`<erni-button kind="white-transparent" fill=\"solid\" role=\"button\" tabindex=\"0\" target=\"_self\">
      <mock:shadow-root>
        <button aria-invalid=\"false\" class=\"erni-button--solid-white-transparent erni-button--md\" kind=\"white-transparent\" tabindex=\"-1\">
          <slot name=\"start\"></slot>
          <slot></slot>
          <slot name=\"end\"></slot>
        </button>
      </mock:shadow-root>
      Render Test
    </erni-button>`);

    done();
  });

  it('Renders dark Button', async done => {
    const cmpRender = await newSpecPage({
      components: [UIButtonComponent],
      html: `<erni-button kind="dark">Render Test</erni-button>`
    });

    await expect(cmpRender.root)
      .toEqualHtml(`<erni-button kind="dark" fill=\"solid\" role=\"button\" tabindex=\"0\" target=\"_self\">
      <mock:shadow-root>
        <button aria-invalid=\"false\" class=\"erni-button--solid-dark erni-button--md\" kind=\"dark\" tabindex=\"-1\">
          <slot name=\"start\"></slot>
          <slot></slot>
          <slot name=\"end\"></slot>
        </button>
      </mock:shadow-root>
      Render Test
    </erni-button>`);

    done();
  });

  it('Renders Primary Anchor', async done => {
    const cmpRender = await newSpecPage({
      components: [UIButtonComponent],
      html: `<erni-button kind="primary" href="https://caixa.es">Render Test</erni-button>`
    });

    await expect(cmpRender.root)
      .toEqualHtml(`<erni-button kind="primary" fill=\"solid\" href=\"https://caixa.es\" role=\"button\" tabindex=\"0\" target=\"_self\">
      <mock:shadow-root>
        <a aria-invalid=\"false\" class=\"erni-button--link-primary erni-button--md\" href=\"https://caixa.es\" tabindex=\"-1\" target=\"_self\">
          <slot name=\"start\"></slot>
          <slot></slot>
          <slot name=\"end\"></slot>
        </a>
      </mock:shadow-root>
      Render Test
    </erni-button>`);

    done();
  });

  it('Renders Secondary Anchor', async done => {
    const cmpRender = await newSpecPage({
      components: [UIButtonComponent],
      html: `<erni-button kind="secondary" href="https://caixa.es">Render Test</erni-button>`
    });

    await expect(cmpRender.root)
      .toEqualHtml(`<erni-button kind="secondary" fill=\"solid\" href=\"https://caixa.es\" role=\"button\" tabindex=\"0\" target=\"_self\">
      <mock:shadow-root>
        <a aria-invalid=\"false\" class=\"erni-button--link-secondary erni-button--md\" href=\"https://caixa.es\" tabindex=\"-1\" target=\"_self\">
          <slot name=\"start\"></slot>
          <slot></slot>
          <slot name=\"end\"></slot>
        </a>
      </mock:shadow-root>
      Render Test
    </erni-button>`);

    done();
  });

  it('Renders tertiary Anchor', async done => {
    const cmpRender = await newSpecPage({
      components: [UIButtonComponent],
      html: `<erni-button kind="tertiary" href="https://caixa.es">Render Test</erni-button>`
    });

    await expect(cmpRender.root)
      .toEqualHtml(`<erni-button kind="tertiary" fill=\"solid\" href=\"https://caixa.es\" role=\"button\" tabindex=\"0\" target=\"_self\">
      <mock:shadow-root>
        <a aria-invalid=\"false\" class=\"erni-button--link-tertiary erni-button--md\" href=\"https://caixa.es\" tabindex=\"-1\" target=\"_self\">
          <slot name=\"start\"></slot>
          <slot></slot>
          <slot name=\"end\"></slot>
        </a>
      </mock:shadow-root>
      Render Test
    </erni-button>`);

    done();
  });

  it('Renders commercial Anchor', async done => {
    const cmpRender = await newSpecPage({
      components: [UIButtonComponent],
      html: `<erni-button kind="commercial" href="https://caixa.es">Render Test</erni-button>`
    });

    await expect(cmpRender.root)
      .toEqualHtml(`<erni-button kind="commercial" fill=\"solid\" href=\"https://caixa.es\" role=\"button\" tabindex=\"0\" target=\"_self\">
      <mock:shadow-root>
        <a aria-invalid=\"false\" class=\"erni-button--link-commercial erni-button--md\" href=\"https://caixa.es\" tabindex=\"-1\" target=\"_self\">
          <slot name=\"start\"></slot>
          <slot></slot>
          <slot name=\"end\"></slot>
        </a>
      </mock:shadow-root>
      Render Test
    </erni-button>`);

    done();
  });

  it('Renders commercial inverse Anchor', async done => {
    const cmpRender = await newSpecPage({
      components: [UIButtonComponent],
      html: `<erni-button kind="commercial-inverse" href="https://caixa.es">Render Test</erni-button>`
    });

    await expect(cmpRender.root)
      .toEqualHtml(`<erni-button kind="commercial-inverse" fill=\"solid\" href=\"https://caixa.es\" role=\"button\" tabindex=\"0\" target=\"_self\">
      <mock:shadow-root>
        <a aria-invalid=\"false\" class=\"erni-button--link-commercial-inverse erni-button--md\" href=\"https://caixa.es\" tabindex=\"-1\" target=\"_self\">
          <slot name=\"start\"></slot>
          <slot></slot>
          <slot name=\"end\"></slot>
        </a>
      </mock:shadow-root>
      Render Test
    </erni-button>`);

    done();
  });

  it('Renders white trasparent Anchor', async done => {
    const cmpRender = await newSpecPage({
      components: [UIButtonComponent],
      html: `<erni-button kind="white-transparent" href="https://caixa.es">Render Test</erni-button>`
    });

    await expect(cmpRender.root)
      .toEqualHtml(`<erni-button kind="white-transparent" fill=\"solid\" href=\"https://caixa.es\" role=\"button\" tabindex=\"0\" target=\"_self\">
      <mock:shadow-root>
        <a aria-invalid=\"false\" class=\"erni-button--link-white-transparent erni-button--md\" href=\"https://caixa.es\" tabindex=\"-1\" target=\"_self\">
          <slot name=\"start\"></slot>
          <slot></slot>
          <slot name=\"end\"></slot>
        </a>
      </mock:shadow-root>
      Render Test
    </erni-button>`);

    done();
  });

  it('Renders Fill link Button', async done => {
    const cmpRender = await newSpecPage({
      components: [UIButtonComponent],
      html: `<erni-button kind="primary" fill="link">Render Test</erni-button>`
    });

    await expect(cmpRender.root)
      .toEqualHtml(`<erni-button kind="primary" fill=\"link\" role=\"button\" tabindex=\"0\" target=\"_self\">
      <mock:shadow-root>
        <button aria-invalid=\"false\" class=\"erni-button--link-primary erni-button--md\" kind=\"primary\" tabindex=\"-1\">
          <slot name=\"start\"></slot>
          <slot></slot>
          <slot name=\"end\"></slot>
        </button>
      </mock:shadow-root>
      Render Test
    </erni-button>`);

    done();
  });

  it('Renders Block Button', async done => {
    const cmpRender = await newSpecPage({
      components: [UIButtonComponent],
      html: `<erni-button kind="primary" block>Render Test</erni-button>`
    });

    await expect(cmpRender.root)
      .toEqualHtml(`<erni-button block="" class="erni-button--block" kind="primary" fill=\"solid\" role=\"button\" tabindex=\"0\" target=\"_self\">
      <mock:shadow-root>
        <button aria-invalid=\"false\" class=\"erni-button--block erni-button--solid-primary erni-button--md\" kind=\"primary\" tabindex=\"-1\">
          <slot name=\"start\"></slot>
          <slot></slot>
          <slot name=\"end\"></slot>
        </button>
      </mock:shadow-root>
      Render Test
    </erni-button>`);

    done();
  });

  it('Renders Disabled Button', async done => {
    const cmpRender = await newSpecPage({
      components: [UIButtonComponent],
      html: `<erni-button kind="primary" disabled>Render Test</erni-button>`
    });

    await expect(cmpRender.root)
      .toEqualHtml(`<erni-button disabled="" kind="primary" fill=\"solid\" role=\"button\" tabindex=\"0\" target=\"_self\">
      <mock:shadow-root>
        <button aria-invalid=\"false\" class=\"erni-button--solid-primary erni-button--md\" disabled="" kind=\"primary\" tabindex=\"-1\">
          <slot name=\"start\"></slot>
          <slot></slot>
          <slot name=\"end\"></slot>
        </button>
      </mock:shadow-root>
      Render Test
    </erni-button>`);

    done();
  });

  it('Renders Rounded shape Button', async done => {
    const cmpRender = await newSpecPage({
      components: [UIButtonComponent],
      html: `<erni-button kind="primary" shape="rounded">Render Test</erni-button>`
    });

    await expect(cmpRender.root)
      .toEqualHtml(`<erni-button kind="primary" shape="rounded" fill=\"solid\" role=\"button\" tabindex=\"0\" target=\"_self\">
      <mock:shadow-root>
        <button aria-invalid=\"false\" class=\"erni-button--rounded erni-button--solid-primary erni-button--md\" kind=\"primary\" tabindex=\"-1\">
          <slot name=\"start\"></slot>
          <slot></slot>
          <slot name=\"end\"></slot>
        </button>
      </mock:shadow-root>
      Render Test
    </erni-button>`);

    done();
  });

  it('Renders Squared shape Button', async done => {
    const cmpRender = await newSpecPage({
      components: [UIButtonComponent],
      html: `<erni-button kind="primary" shape="squared">Render Test</erni-button>`
    });

    await expect(cmpRender.root)
      .toEqualHtml(`<erni-button kind="primary" shape="squared" fill=\"solid\" role=\"button\" tabindex=\"0\" target=\"_self\">
      <mock:shadow-root>
        <button aria-invalid=\"false\" class=\"erni-button--squared erni-button--solid-primary erni-button--md\" kind=\"primary\" tabindex=\"-1\">
          <slot name=\"start\"></slot>
          <slot></slot>
          <slot name=\"end\"></slot>
        </button>
      </mock:shadow-root>
      Render Test
    </erni-button>`);

    done();
  });

  it('Renders Extrawide width Button', async done => {
    const cmpRender = await newSpecPage({
      components: [UIButtonComponent],
      html: `<erni-button kind="primary" width-type="extrawide">Render Test</erni-button>`
    });

    await expect(cmpRender.root)
      .toEqualHtml(`<erni-button kind="primary" fill=\"solid\" role=\"button\" tabindex=\"0\" target=\"_self\" width-type="extrawide">
      <mock:shadow-root>
        <button aria-invalid=\"false\" class=\"erni-button--extrawide erni-button--solid-primary erni-button--md\" kind=\"primary\" tabindex=\"-1\">
          <slot name=\"start\"></slot>
          <slot></slot>
          <slot name=\"end\"></slot>
        </button>
      </mock:shadow-root>
      Render Test
    </erni-button>`);

    done();
  });

  it('Renders fixed width Button', async done => {
    const cmpRender = await newSpecPage({
      components: [UIButtonComponent],
      html: `<erni-button kind="primary" width-type="fixed">Render Test</erni-button>`
    });

    await expect(cmpRender.root)
      .toEqualHtml(`<erni-button kind="primary" fill=\"solid\" role=\"button\" tabindex=\"0\" target=\"_self\" width-type="fixed">
      <mock:shadow-root>
        <button aria-invalid=\"false\" class=\"erni-button--fixed erni-button--solid-primary erni-button--md\" kind=\"primary\" tabindex=\"-1\">
          <slot name=\"start\"></slot>
          <slot></slot>
          <slot name=\"end\"></slot>
        </button>
      </mock:shadow-root>
      Render Test
    </erni-button>`);

    done();
  });

  it('Renders extra padding Button', async done => {
    const cmpRender = await newSpecPage({
      components: [UIButtonComponent],
      html: `<erni-button kind="primary" extra-padding>Render Test</erni-button>`
    });

    await expect(cmpRender.root)
      .toEqualHtml(`<erni-button kind="primary" fill=\"solid\" role=\"button\" tabindex=\"0\" target=\"_self\" extra-padding="">
      <mock:shadow-root>
        <button aria-invalid=\"false\" class=\"erni-button--extrapadding erni-button--solid-primary erni-button--md\" kind=\"primary\" tabindex=\"-1\">
          <slot name=\"start\"></slot>
          <slot></slot>
          <slot name=\"end\"></slot>
        </button>
      </mock:shadow-root>
      Render Test
    </erni-button>`);

    done();
  });

  it('Renders sm Button', async done => {
    const cmpRender = await newSpecPage({
      components: [UIButtonComponent],
      html: `<erni-button kind="primary" size="sm">Render Test</erni-button>`
    });

    await expect(cmpRender.root)
      .toEqualHtml(`<erni-button kind="primary" fill=\"solid\" role=\"button\" size="sm" tabindex=\"0\" target=\"_self\">
      <mock:shadow-root>
        <button aria-invalid=\"false\" class=\"erni-button--solid-primary erni-button--sm\" kind=\"primary\" tabindex=\"-1\">
          <slot name=\"start\"></slot>
          <slot></slot>
          <slot name=\"end\"></slot>
        </button>
      </mock:shadow-root>
      Render Test
    </erni-button>`);

    done();
  });

  it('Renders md Button', async done => {
    const cmpRender = await newSpecPage({
      components: [UIButtonComponent],
      html: `<erni-button kind="primary" size="md">Render Test</erni-button>`
    });

    await expect(cmpRender.root)
      .toEqualHtml(`<erni-button kind="primary" fill=\"solid\" role=\"button\" size="md" tabindex=\"0\" target=\"_self\">
      <mock:shadow-root>
        <button aria-invalid=\"false\" class=\"erni-button--solid-primary erni-button--md\" kind=\"primary\" tabindex=\"-1\">
          <slot name=\"start\"></slot>
          <slot></slot>
          <slot name=\"end\"></slot>
        </button>
      </mock:shadow-root>
      Render Test
    </erni-button>`);

    done();
  });

  it('Renders lg Button', async done => {
    const cmpRender = await newSpecPage({
      components: [UIButtonComponent],
      html: `<erni-button kind="primary" size="lg">Render Test</erni-button>`
    });

    await expect(cmpRender.root)
      .toEqualHtml(`<erni-button kind="primary" fill=\"solid\" role=\"button\" size="lg" tabindex=\"0\" target=\"_self\">
      <mock:shadow-root>
        <button aria-invalid=\"false\" class=\"erni-button--solid-primary erni-button--lg\" kind=\"primary\" tabindex=\"-1\">
          <slot name=\"start\"></slot>
          <slot></slot>
          <slot name=\"end\"></slot>
        </button>
      </mock:shadow-root>
      Render Test
    </erni-button>`);

    done();
  });

  it('Renders xl Button', async done => {
    const cmpRender = await newSpecPage({
      components: [UIButtonComponent],
      html: `<erni-button kind="primary" size="xl">Render Test</erni-button>`
    });

    await expect(cmpRender.root)
      .toEqualHtml(`<erni-button kind="primary" fill=\"solid\" role=\"button\" size="xl" tabindex=\"0\" target=\"_self\">
      <mock:shadow-root>
        <button aria-invalid=\"false\" class=\"erni-button--solid-primary erni-button--xl\" kind=\"primary\" tabindex=\"-1\">
          <slot name=\"start\"></slot>
          <slot></slot>
          <slot name=\"end\"></slot>
        </button>
      </mock:shadow-root>
      Render Test
    </erni-button>`);

    done();
  });
});
