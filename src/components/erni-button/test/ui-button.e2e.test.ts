import { newE2EPage } from '@stencil/core/testing';

describe('erni-button', () => {
  it('event on click the button', async () => {
    const page = await newE2EPage();

    await page.setContent(`
    <erni-button>Hola Click Me!</erni-button>
  `);

    const componentClick = await page.spyOnEvent('erniButtonClicked');
    const btn = await page.find('erni-button');
    btn.click();

    await page.waitForChanges();
    await expect(componentClick).toHaveReceivedEventDetail({
      value: true
    });
  });

  it('on tab active element should the next element', async () => {
    const page = await newE2EPage();

    await page.setContent(
      `<erni-button id="start">Hola Click Me!</erni-button><erni-button  id="end">Hola Click Me 2 !</erni-button>`
    );

    await page.keyboard.press('Tab');
    await page.keyboard.press('Tab');
    await page.waitForChanges();
    const el = await page.evaluateHandle(() => document.activeElement);
    await expect(await el._remoteObject.description).toContain('#end');
  });
});
