### Sizes

::: demo [vanilla]

```html
<html>
  <!-- Small  -->
  <erni-button size="sm">Button sm</erni-button>
  <!-- Medium (Default) -->
  <erni-button size="md">Button md</erni-button>
  <!-- Large -->
  <erni-button size="lg">Button lg</erni-button>
  <!-- Extra Large -->
  <erni-button size="xl">Button xl</erni-button>
  <!-- Block -->
  <erni-button block>Button Block</erni-button>
</html>
```

:::

### Kind

::: demo [vanilla]

```html
<html>
  <!-- Primary  -->
  <erni-button kind="primary" size="md" disabled>Disabled</erni-button>
  <erni-button kind="primary">Button Default</erni-button>
  <erni-button kind="primary" size="sm">Button sm</erni-button>
  <erni-button kind="primary" size="md">Button md</erni-button>
  <erni-button kind="primary" size="lg">Button lg</erni-button>
  <erni-button kind="primary" block>Button Block</erni-button>
  <erni-button kind="primary" fill="link">Fill Link</erni-button>

  <!-- Secondary  -->
  <erni-button kind="secondary" size="md" disabled>Disabled</erni-button>
  <erni-button kind="secondary">Button Default</erni-button>
  <erni-button kind="secondary" size="sm">Button sm</erni-button>
  <erni-button kind="secondary" size="md">Button md</erni-button>
  <erni-button kind="secondary" size="lg">Button lg</erni-button>
  <erni-button kind="secondary" block>Button Block</erni-button>
  <erni-button kind="secondary" fill="link">Fill Link</erni-button>

  <!-- Tertiary  -->
  <erni-button kind="tertiary" size="md" disabled>Disabled</erni-button>
  <erni-button kind="tertiary">Button Default</erni-button>
  <erni-button kind="tertiary" size="sm">Button sm</erni-button>
  <erni-button kind="tertiary" size="md">Button md</erni-button>
  <erni-button kind="tertiary" size="lg">Button lg</erni-button>
  <erni-button kind="tertiary" block>Button Block</erni-button>
  <erni-button kind="tertiary" fill="link">Fill Link</erni-button>

  <!-- Commercial  -->
  <erni-button kind="commercial" size="md" disabled>Disabled</erni-button>
  <erni-button kind="commercial">Button Default</erni-button>
  <erni-button kind="commercial" size="sm">Button sm</erni-button>
  <erni-button kind="commercial" size="md">Button md</erni-button>
  <erni-button kind="commercial" size="lg">Button lg</erni-button>
  <erni-button kind="commercial" block>Button Block</erni-button>
  <erni-button kind="commercial" fill="link">Fill Link</erni-button>

  <!-- Commercial inverse  -->
  <erni-button kind="commercial-inverse" size="md" disabled>Disabled</erni-button>
  <erni-button kind="commercial-inverse">Button Default</erni-button>
  <erni-button kind="commercial-inverse" size="sm">Button sm</erni-button>
  <erni-button kind="commercial-inverse" size="md">Button md</erni-button>
  <erni-button kind="commercial-inverse" size="lg">Button lg</erni-button>
  <erni-button kind="commercial-inverse" block>Button Block</erni-button>
  <erni-button kind="commercial-inverse" fill="link">Fill Link</erni-button>

  <!-- White Transparent -->
  <erni-button kind="white-transparent" size="md" disabled>Disabled</erni-button>
  <erni-button kind="white-transparent">Button Default</erni-button>
  <erni-button kind="white-transparent" size="sm">Button sm</erni-button>
  <erni-button kind="white-transparent" size="md">Button md</erni-button>
  <erni-button kind="white-transparent" size="lg">Button lg</erni-button>
  <erni-button kind="white-transparent" block>Button Block</erni-button>
  <erni-button kind="white-transparent" fill="link">Fill Link</erni-button>

  <!-- Dark  -->
  <erni-button kind="dark" size="md" disabled>Disabled</erni-button>
  <erni-button kind="dark">Button Default</erni-button>
  <erni-button kind="dark" size="sm">Button sm</erni-button>
  <erni-button kind="dark" size="md">Button md</erni-button>
  <erni-button kind="dark" size="lg">Button lg</erni-button>
  <erni-button kind="dark" block>Button Block</erni-button>
  <erni-button kind="dark" fill="link">Fill Link</erni-button>
</html>
```

:::

### Button With Link

::: demo [vanilla]

```html
<html>
  <!-- Primary  -->
  <erni-button size="sm" href="http://www.google.com">Link sm</erni-button>
  <erni-button size="md" href="http://www.google.com">Link md</erni-button>
  <erni-button size="lg" href="http://www.google.com">Link lg</erni-button>
  <erni-button size="xl" href="http://www.google.com">Link lg</erni-button>

  <!-- secondary  -->
  <erni-button kind="secondary" size="sm" href="http://www.google.com">Link sm</erni-button>
  <erni-button kind="secondary" size="md" href="http://www.google.com">Link md</erni-button>
  <erni-button kind="secondary" size="lg" href="http://www.google.com">Link lg</erni-button>
  <erni-button kind="secondary" size="xl" href="http://www.google.com">Link xl</erni-button>

  <!-- tertiary  -->
  <erni-button kind="tertiary" size="sm" href="http://www.google.com">Link sm</erni-button>
  <erni-button kind="tertiary" size="md" href="http://www.google.com">Link md</erni-button>
  <erni-button kind="tertiary" size="lg" href="http://www.google.com">Link lg</erni-button>
  <erni-button kind="tertiary" size="xl" href="http://www.google.com">Link xl</erni-button>

  <!-- commercial  -->
  <erni-button kind="commercial" size="sm" href="http://www.google.com">Link sm</erni-button>
  <erni-button kind="commercial" size="md" href="http://www.google.com">Link md</erni-button>
  <erni-button kind="commercial" size="lg" href="http://www.google.com">Link lg</erni-button>
  <erni-button kind="commercial" size="xl" href="http://www.google.com">Link xl</erni-button>

  <!-- commercial inverse -->
  <erni-button kind="commercial-inverse" size="sm" href="http://www.google.com">Link sm</erni-button>
  <erni-button kind="commercial-inverse" size="md" href="http://www.google.com">Link md</erni-button>
  <erni-button kind="commercial-inverse" size="lg" href="http://www.google.com">Link lg</erni-button>
  <erni-button kind="commercial-inverse" size="xl" href="http://www.google.com">Link xl</erni-button>

  <!-- White Transparent  -->
  <erni-button kind="white-transparent" size="sm" href="http://www.google.com">Link sm</erni-button>
  <erni-button kind="white-transparent" size="md" href="http://www.google.com">Link md</erni-button>
  <erni-button kind="white-transparent" size="lg" href="http://www.google.com">Link lg</erni-button>
  <erni-button kind="white-transparent" size="xl" href="http://www.google.com">Link xl</erni-button>

  <!-- Dark  -->
  <erni-button kind="dark" size="sm" href="http://www.google.com">Link sm</erni-button>
  <erni-button kind="dark" size="md" href="http://www.google.com">Link md</erni-button>
  <erni-button kind="dark" size="lg" href="http://www.google.com">Link lg</erni-button>
  <erni-button kind="dark" size="xl" href="http://www.google.com">Link xl</erni-button>
</html>
```

:::

### Link

::: demo [vanilla]

```html
<html>
  <!-- Primary  -->
  <erni-button fill="link" size="sm">Link sm</erni-button>
  <erni-button fill="link" size="md">Link md</erni-button>
  <erni-button fill="link" size="lg">Link lg</erni-button>
  <erni-button fill="link" size="xl">Link lg</erni-button>

  <!-- secondary  -->
  <erni-button fill="link" kind="secondary" size="sm">Link sm</erni-button>
  <erni-button fill="link" kind="secondary" size="md">Link md</erni-button>
  <erni-button fill="link" kind="secondary" size="lg">Link lg</erni-button>
  <erni-button fill="link" kind="secondary" size="xl">Link xl</erni-button>

  <!-- tertiary  -->
  <erni-button fill="link" kind="tertiary" size="sm">Link sm</erni-button>
  <erni-button fill="link" kind="tertiary" size="md">Link md</erni-button>
  <erni-button fill="link" kind="tertiary" size="lg">Link lg</erni-button>
  <erni-button fill="link" kind="tertiary" size="xl">Link xl</erni-button>

  <!-- commercial  -->
  <erni-button fill="link" kind="commercial" size="sm">Link sm</erni-button>
  <erni-button fill="link" kind="commercial" size="md">Link md</erni-button>
  <erni-button fill="link" kind="commercial" size="lg">Link lg</erni-button>
  <erni-button fill="link" kind="commercial" size="xl">Link xl</erni-button>

  <!-- commercial inverse -->
  <erni-button fill="link" kind="commercial-inverse" size="sm">Link sm</erni-button>
  <erni-button fill="link" kind="commercial-inverse" size="md">Link md</erni-button>
  <erni-button fill="link" kind="commercial-inverse" size="lg">Link lg</erni-button>
  <erni-button fill="link" kind="commercial-inverse" size="xl">Link xl</erni-button>

  <!-- White Transparent  -->
  <erni-button fill="link" kind="white-transparent" size="sm">Link sm</erni-button>
  <erni-button fill="link" kind="white-transparent" size="md">Link md</erni-button>
  <erni-button fill="link" kind="white-transparent" size="lg">Link lg</erni-button>
  <erni-button fill="link" kind="white-transparent" size="xl">Link xl</erni-button>

  <!-- Dark  -->
  <erni-button fill="link" kind="dark" size="sm">Link sm</erni-button>
  <erni-button fill="link" kind="dark" size="md">Link md</erni-button>
  <erni-button fill="link" kind="dark" size="lg">Link lg</erni-button>
  <erni-button fill="link" kind="dark" size="xl">Link xl</erni-button>
</html>
```

:::

::: warning
Differences between fill `link` and a button with `href` are mostly in the way the will behavior and tag assigned to them. The `erni-button` with attribute `href` will be an HTMLElement `anchor` and visually has an underline text decoration. `erni-button`'s using the attribute `fill="link"` will be HTMLElement `button` but looking similar to a link, without borders but with button functionality.
:::

### Shape

::: demo [vanilla]

```html
<html>
  <!-- Rounded -->
  <erni-button size="md" shape="rounded">Rounded</erni-button>
  <!-- Squared -->
  <erni-button size="md" shape="squared">Squared</erni-button>
</html>
```

:::

### Width Type

::: demo [vanilla]

```html
<html>
  <!-- Fixed -->
  <erni-button size="md" width-type="fixed">Fixed</erni-button>
  <erni-button size="md" shape="rounded" width-type="fixed">Rounded Fixed</erni-button>
  <!-- Extrawide -->
  <erni-button size="md" width-type="extrawide">Extrawide</erni-button>
</html>
```

:::

### Extra Horizontal Padding

::: demo [vanilla]

```html
<html>
  <!-- Extra padding -->
  <erni-button size="md" extra-padding="true">Extra padding</erni-button>
</html>
```

:::

### Slots

::: demo [vanilla]

```html
<html>
  <erni-button kind="secondary" size="lg">
    <span slot="start">Prefix Text -- </span>
    Button Content
    <span slot="end"> -- Suffix Text</span>
  </erni-button>

  <erni-button kind="secondary" size="lg">
    <ui-icon name="keyboard-arrow-left" size="s" slot="start"></ui-icon>
    Button Content
    <ui-icon name="keyboard-arrow-right" size="s" slot="end"></ui-icon>
  </erni-button>
</html>
```

:::
