**Use erni-button as react component**

```jsx
import { ErniButton } from '@erni/components/react';

<ErniButton>Hola</ErniButton>
<ErniButton kind="secondary" size="md">Button md</ErniButton>
```
